CREATE TABLE accounts
(
    id         BIGSERIAL PRIMARY KEY,
    first_name varchar(255),
    last_name  varchar(255),
    pesel      varchar(11)
);

CREATE TABLE bills
(
    id         BIGSERIAL PRIMARY KEY,
    currency   varchar(255),
    amount     varchar(255),
    id_account bigint references accounts (id)
);
