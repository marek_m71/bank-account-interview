package com.interview.bank.account.service;

import static org.junit.Assert.*;

import com.interview.bank.account.dto.in.AccountCreateRequest;
import com.interview.bank.account.dto.out.AccountResponse;
import java.math.BigDecimal;
import javax.transaction.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceTest {

  private static final String PESEL = "90000000000";

  @Autowired
  private AccountService accountService;

  @Test
  @Transactional
  public void createAccount() {
    AccountCreateRequest request = buildAccountCreateRequest(PESEL);
    AccountResponse response = accountService.createAccount(request);
    assertEquals(PESEL, response.getPesel());
  }

  @Test
  @Transactional
  public void getAccount() {
    accountService.createAccount(buildAccountCreateRequest(PESEL));

    AccountResponse response = accountService.getAccount(PESEL);
    assertEquals(PESEL, response.getPesel());
  }


  private AccountCreateRequest buildAccountCreateRequest(String pesel) {
    AccountCreateRequest request = new AccountCreateRequest();
    request.setPesel(pesel);
    request.setAmount(BigDecimal.ONE);
    return request;
  }
}
