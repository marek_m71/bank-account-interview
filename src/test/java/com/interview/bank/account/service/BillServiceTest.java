package com.interview.bank.account.service;

import static org.junit.Assert.*;

import com.interview.bank.account.configuration.exeception.AmountNotEnoughException;
import com.interview.bank.account.db.Bill;
import com.interview.bank.account.db.enums.Currency;
import java.math.BigDecimal;
import org.junit.Test;

public class BillServiceTest {

  private final BillService billService = new BillService();

  @Test
  public void plusAmount() {
    Bill bill = buildBill(BigDecimal.valueOf(100));
    billService.plusAmount(bill, BigDecimal.valueOf(100));

    assertEquals(bill.getAmount(), BigDecimal.valueOf(200));
  }

  @Test
  public void minusAmount() {
    Bill bill = buildBill(BigDecimal.valueOf(200));
    billService.minusAmount(bill, BigDecimal.valueOf(100));

    assertEquals(bill.getAmount(), BigDecimal.valueOf(100));
  }

  @Test(expected = AmountNotEnoughException.class)
  public void minusAmountException() {
    Bill bill = buildBill(BigDecimal.valueOf(200));
    billService.minusAmount(bill, BigDecimal.valueOf(300));
  }

  private Bill buildBill(BigDecimal amount){
    Bill bill = new Bill();
    bill.setCurrency(Currency.PLN);
    bill.setAmount(amount);
    return bill;
  }
}
