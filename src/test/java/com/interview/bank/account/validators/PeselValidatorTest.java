package com.interview.bank.account.validators;

import static org.junit.Assert.*;

import org.junit.Test;

public class PeselValidatorTest {

  PeselValidator peselValidator = new PeselValidator();

  @Test
  public void isValid() {
    String pesel = "75031215248";
    boolean isValid = peselValidator.isValid(pesel, null);
    assertTrue(isValid);
  }

  @Test
  public void isNotValidLength() {
    String pesel = "7503215248";
    boolean isValid = peselValidator.isValid(pesel, null);
    assertFalse(isValid);
  }
}
