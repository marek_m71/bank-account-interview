package com.interview.bank.account.validators;

import static org.junit.Assert.*;

import org.junit.Test;

public class AdultValidatorTest {

  AdultValidator adultValidator = new AdultValidator();

  @Test
  public void isAdult() {
    String pesel = "75031215248";
    boolean isValid = adultValidator.isValid(pesel, null);
    assertTrue(isValid);
  }

  @Test
  public void isNotAdult() {
    String pesel = "09292087168";
    boolean isValid = adultValidator.isValid(pesel, null);
    assertFalse(isValid);
  }
}
