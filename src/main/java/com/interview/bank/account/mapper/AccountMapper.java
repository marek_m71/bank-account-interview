package com.interview.bank.account.mapper;

import com.interview.bank.account.db.Account;
import com.interview.bank.account.dto.in.AccountCreateRequest;
import com.interview.bank.account.dto.out.AccountResponse;
import org.mapstruct.Mapper;

@Mapper(uses = BillMapper.class)
public interface AccountMapper {

  Account map(AccountCreateRequest in);

  AccountResponse map(Account in);
}
