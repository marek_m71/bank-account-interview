package com.interview.bank.account.mapper;

import com.interview.bank.account.db.Bill;
import com.interview.bank.account.dto.out.BillResponse;
import org.mapstruct.Mapper;

@Mapper
public interface BillMapper {

  BillResponse map(Bill in);
}
