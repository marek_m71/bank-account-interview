package com.interview.bank.account.validators;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = PeselValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Pesel {

  String INVALID = "pesel is not valid";

  String message() default INVALID;

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}

