package com.interview.bank.account.validators;

import com.interview.bank.account.utils.PeselUtils;
import java.time.LocalDate;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.stereotype.Component;

@Component
public class AdultValidator implements ConstraintValidator<Adult, String> {

  public AdultValidator() {
  }

  @Override
  public boolean isValid(String pesel, ConstraintValidatorContext context) {
    if (!PeselUtils.isValid(pesel)) {
      return false;
    }

    final LocalDate from = LocalDate
        .of(PeselUtils.getBirthYear(pesel), PeselUtils.getBirthMonth(pesel),
            PeselUtils.getBirthDay(pesel));

    final LocalDate now = LocalDate.now();

    return now.minusYears(18).isAfter(from);
  }
}
