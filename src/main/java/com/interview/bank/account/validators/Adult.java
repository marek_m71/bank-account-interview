package com.interview.bank.account.validators;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = AdultValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Adult {

  String INVALID = "the person is not of legal age";

  String message() default INVALID;

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}

