package com.interview.bank.account.validators;

import com.interview.bank.account.utils.PeselUtils;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.stereotype.Component;

@Component
public class PeselValidator implements ConstraintValidator<Pesel, String> {

  public PeselValidator() {
  }

  @Override
  public boolean isValid(String pesel, ConstraintValidatorContext context) {
    return PeselUtils.isValid(pesel);
  }
}
