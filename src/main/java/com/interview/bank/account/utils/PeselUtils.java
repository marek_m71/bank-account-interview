package com.interview.bank.account.utils;

public class PeselUtils {

  public static boolean isValid(String pesel) {

    if (pesel == null) {
      return false;
    }

    if (pesel.length() != 11) {
      return false;
    }

    return getDigitFromPos(pesel, 10).equals(getChecksum(pesel));
  }

  public static Integer getBirthYear(String pesel) {
    int year = 10 * getDigitFromPos(pesel, 0).intValue();
    year += getDigitFromPos(pesel, 1);

    int month = 10 * getDigitFromPos(pesel, 2).intValue();
    month += getDigitFromPos(pesel, 3);

    if (month > 80 && month < 93) {
      year += 1800;
    } else if (month > 0 && month < 13) {
      year += 1900;
    } else if (month > 20 && month < 33) {
      year += 2000;
    } else if (month > 40 && month < 53) {
      year += 2100;
    } else if (month > 60 && month < 73) {
      year += 2200;
    }
    return year;
  }

  public static int getBirthMonth(String pesel) {
    int month = 10 * getDigitFromPos(pesel, 2).intValue();
    month += getDigitFromPos(pesel, 3);
    if (month > 80 && month < 93) {
      month -= 80;
    } else if (month > 20 && month < 33) {
      month -= 20;
    } else if (month > 40 && month < 53) {
      month -= 40;
    } else if (month > 60 && month < 73) {
      month -= 60;
    }
    return month;
  }

  public static int getBirthDay(String pesel) {
    int day = 10 * getDigitFromPos(pesel, 4).intValue();
    day += getDigitFromPos(pesel, 5);

    return day;
  }

  private static Long getChecksum(String pesel) {
    long[] tab = new long[11];
    long[] weights = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1};

    for (int i = 0; i < 11; i++) {
      tab[i] = getDigitFromPos(pesel, i);
    }

    long sum = 0;
    for (int i = 0; i < 10; i++) {
      sum += weights[i] * tab[i];
    }
    sum = 10 - (sum % 10);
    return sum % 10;
  }

  private static Long getDigitFromPos(String number, Integer pos) {
    return Long.parseLong(number.substring(pos, pos + 1));
  }
}
