package com.interview.bank.account.configuration.exeception;

public class NotAllowedOperationException extends RuntimeException {

  public NotAllowedOperationException(String currency) {
    super("Not exchange money for: " + currency + ", not allowed operation");
  }
}
