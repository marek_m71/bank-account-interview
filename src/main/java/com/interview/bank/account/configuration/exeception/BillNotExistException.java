package com.interview.bank.account.configuration.exeception;

import com.interview.bank.account.db.enums.Currency;

public class BillNotExistException extends RuntimeException {

  public BillNotExistException(Currency currency) {
    super("Bill with currency: " + currency.toString() + " doesn't exist");
  }
}
