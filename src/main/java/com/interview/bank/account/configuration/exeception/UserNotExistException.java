package com.interview.bank.account.configuration.exeception;

public class UserNotExistException extends RuntimeException {

  public UserNotExistException(String pesel) {
    super("User with pesel: " + pesel + " doesn't exist");
  }
}
