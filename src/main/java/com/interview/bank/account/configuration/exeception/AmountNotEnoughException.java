package com.interview.bank.account.configuration.exeception;

public class AmountNotEnoughException extends RuntimeException {

  public AmountNotEnoughException() {
    super("Amount not enough to exchange");
  }
}
