package com.interview.bank.account.configuration.exeception.advices;

import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.AdviceTrait;

public interface FeignClientErrorAdviceTrait extends AdviceTrait {

  @ExceptionHandler
  default ResponseEntity<Problem> handleFeignException(final FeignException e,
      final NativeWebRequest request) {
    Logger logger = LoggerFactory.getLogger(FeignClientErrorAdviceTrait.class);

    if (logger.isErrorEnabled()) {
      logger.error("Feign client exception, status: {}, message: {}, body: {}", e.status(),
          e.getMessage(), e.contentUTF8());
    }

    return create(Status.valueOf(e.status()), e, request);
  }
}
