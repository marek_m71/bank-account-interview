package com.interview.bank.account.configuration.exeception.advices;

import com.interview.bank.account.configuration.exeception.UserNotExistException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.AdviceTrait;

public interface UserNotExistAdviceTrait extends AdviceTrait {

  @ExceptionHandler
  default ResponseEntity<Problem> handleUserNotExistExceptions(final UserNotExistException e,
      final NativeWebRequest request) {
    Logger logger = LoggerFactory.getLogger(UserNotExistException.class);

    if (logger.isErrorEnabled()) {
      logger.error("User doesn't exist, message: {}", e.getMessage());
    }

    return create(Status.BAD_REQUEST, e, request);
  }
}
