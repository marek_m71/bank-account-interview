package com.interview.bank.account.configuration.exeception.advices;

import com.interview.bank.account.configuration.exeception.NotAllowedOperationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.AdviceTrait;

public interface NotAllowedOperationAdviceTrait extends AdviceTrait {

  @ExceptionHandler
  default ResponseEntity<Problem> handleNotAllowedOperationExceptions(
      final NotAllowedOperationException e,
      final NativeWebRequest request) {
    Logger logger = LoggerFactory.getLogger(NotAllowedOperationException.class);

    if (logger.isErrorEnabled()) {
      logger.error("Not allowed operation, message: {}", e.getMessage());
    }

    return create(Status.BAD_REQUEST, e, request);
  }
}
