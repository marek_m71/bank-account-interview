package com.interview.bank.account.configuration.exeception.handler;

import com.interview.bank.account.configuration.exeception.advices.AmountNotEnoughAdviceTrait;
import com.interview.bank.account.configuration.exeception.advices.BillNotExistAdviceTrait;
import com.interview.bank.account.configuration.exeception.advices.FeignClientErrorAdviceTrait;
import com.interview.bank.account.configuration.exeception.advices.NotAllowedOperationAdviceTrait;
import com.interview.bank.account.configuration.exeception.advices.ProblemWithExchangeRateAdviceTrait;
import com.interview.bank.account.configuration.exeception.advices.UserExistAdviceTrait;
import com.interview.bank.account.configuration.exeception.advices.UserNotExistAdviceTrait;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.zalando.problem.spring.web.advice.ProblemHandling;

@ControllerAdvice
class ExceptionHandling implements ProblemHandling, FeignClientErrorAdviceTrait,
    UserExistAdviceTrait, UserNotExistAdviceTrait, BillNotExistAdviceTrait,
    AmountNotEnoughAdviceTrait, NotAllowedOperationAdviceTrait, ProblemWithExchangeRateAdviceTrait {

}

