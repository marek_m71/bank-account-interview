package com.interview.bank.account.configuration.exeception;

import com.interview.bank.account.db.enums.Currency;

public class ProblemWithExchangeRateException extends RuntimeException {

  public ProblemWithExchangeRateException(Currency currency) {
    super("Problem with download exchange rate for: " + currency.toString());
  }
}
