package com.interview.bank.account.configuration.exeception;

public class UserExistException extends RuntimeException {

  public UserExistException(String pesel) {
    super("User with pesel: " + pesel + " already exist");
  }
}
