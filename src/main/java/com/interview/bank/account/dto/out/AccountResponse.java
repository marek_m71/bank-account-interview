package com.interview.bank.account.dto.out;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AccountResponse {

  private Long id;
  private String pesel;
  private String firstName;
  private String lastName;
  private List<BillResponse> bills;
}
