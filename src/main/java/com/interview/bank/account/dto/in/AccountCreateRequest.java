package com.interview.bank.account.dto.in;

import com.interview.bank.account.validators.Adult;
import com.interview.bank.account.validators.Pesel;
import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AccountCreateRequest {

  @NotBlank
  @Adult
  @Pesel
  private String pesel;
  @NotBlank
  private String firstName;
  @NotBlank
  private String lastName;
  @NotNull
  private BigDecimal amount;
}
