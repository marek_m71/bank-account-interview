package com.interview.bank.account.dto.provider.out;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class NbpResponse {

  private String table;
  private String currency;
  private String code;
  private List<NbpRateResponse> rates = null;
}
