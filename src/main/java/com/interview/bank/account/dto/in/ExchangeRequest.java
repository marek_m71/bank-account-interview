package com.interview.bank.account.dto.in;

import com.interview.bank.account.db.enums.Currency;
import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ExchangeRequest {

  @NotBlank
  private String pesel;
  @NotNull
  private BigDecimal amount;
  @NotNull
  private Currency from;
  @NotNull
  private Currency to;
}
