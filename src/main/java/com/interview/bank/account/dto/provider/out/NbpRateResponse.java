package com.interview.bank.account.dto.provider.out;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class NbpRateResponse {

  private String no;
  private String effectiveDate;
  private Double mid;
}
