package com.interview.bank.account.dto.out;

import com.interview.bank.account.db.enums.Currency;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BillResponse {

  private Long id;
  private Currency currency;
  private BigDecimal amount;
}
