package com.interview.bank.account.service;

import com.interview.bank.account.configuration.exeception.NotAllowedOperationException;
import com.interview.bank.account.configuration.exeception.ProblemWithExchangeRateException;
import com.interview.bank.account.configuration.exeception.UserExistException;
import com.interview.bank.account.configuration.exeception.UserNotExistException;
import com.interview.bank.account.db.Account;
import com.interview.bank.account.db.Bill;
import com.interview.bank.account.db.enums.Currency;
import com.interview.bank.account.dto.in.AccountCreateRequest;
import com.interview.bank.account.dto.in.ExchangeRequest;
import com.interview.bank.account.dto.out.AccountResponse;
import com.interview.bank.account.dto.provider.out.NbpRateResponse;
import com.interview.bank.account.mapper.AccountMapper;
import com.interview.bank.account.provider.NbpClient;
import com.interview.bank.account.repository.AccountRepository;
import java.math.BigDecimal;
import java.math.RoundingMode;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

  private final AccountMapper accountMapper;
  private final AccountRepository accountRepository;
  private final BillService billService;
  private final NbpClient nbpClient;

  public AccountService(AccountMapper accountMapper,
      AccountRepository accountRepository,
      BillService billService, NbpClient nbpClient) {
    this.accountMapper = accountMapper;
    this.accountRepository = accountRepository;
    this.billService = billService;
    this.nbpClient = nbpClient;
  }

  public AccountResponse createAccount(AccountCreateRequest request) {
    if (accountRepository.findByPesel(request.getPesel()).isPresent()) {
      throw new UserExistException(request.getPesel());
    }

    Account account = accountMapper.map(request);
    Bill bill = billService.findOrCreateBill(account, Currency.PLN);
    billService.plusAmount(bill, request.getAmount());

    return accountMapper.map(accountRepository.save(account));
  }

  public AccountResponse getAccount(String pesel) {
    Account account = accountRepository.findByPesel(pesel)
        .orElseThrow(() -> new UserNotExistException(pesel));

    return accountMapper.map(account);
  }

  public AccountResponse exchange(ExchangeRequest request) {
    if (!(request.getFrom().equals(Currency.PLN) || request.getTo().equals(Currency.PLN))
        || (request.getFrom().equals(request.getTo()))) {
      throw new NotAllowedOperationException(request.getFrom() + "/" + request.getTo());
    }

    Currency exchangeCurrency =
        request.getFrom().equals(Currency.PLN) ? request.getTo() : request.getFrom();

    BigDecimal exchangeRate = BigDecimal
        .valueOf(nbpClient.getRates(exchangeCurrency.toString()).getRates().stream()
            .findFirst()
            .map(NbpRateResponse::getMid)
            .orElseThrow(() -> new ProblemWithExchangeRateException(exchangeCurrency)));

    Account account = accountRepository.findByPesel(request.getPesel())
        .orElseThrow(() -> new UserNotExistException(request.getPesel()));

    Bill billFrom = billService.findBill(account, request.getFrom());
    billService.minusAmount(billFrom, request.getAmount());

    Bill billTo = billService.findOrCreateBill(account, request.getTo());
    if (request.getTo().equals(Currency.PLN)) {
      billService.plusAmount(billTo, request.getAmount().multiply(exchangeRate));
    } else {
      billService
          .plusAmount(billTo, request.getAmount().divide(exchangeRate, RoundingMode.HALF_UP));
    }

    return accountMapper.map(accountRepository.save(account));
  }
}
