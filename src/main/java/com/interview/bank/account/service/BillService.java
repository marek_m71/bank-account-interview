package com.interview.bank.account.service;

import com.interview.bank.account.configuration.exeception.AmountNotEnoughException;
import com.interview.bank.account.configuration.exeception.BillNotExistException;
import com.interview.bank.account.db.Account;
import com.interview.bank.account.db.Bill;
import com.interview.bank.account.db.enums.Currency;
import java.math.BigDecimal;
import org.springframework.stereotype.Service;

@Service
public class BillService {

  public Bill findOrCreateBill(Account account, Currency currency) {
    return account.getBills().stream().filter(r -> r.getCurrency().equals(currency))
        .findFirst().orElseGet(() -> {
          Bill bill = new Bill(currency);
          account.addBill(bill);
          return bill;
        });
  }

  public Bill findBill(Account account, Currency currency) {
    return account.getBills().stream().filter(r -> r.getCurrency().equals(currency))
        .findFirst().orElseThrow(() -> new BillNotExistException(currency));
  }

  public void plusAmount(Bill bill, BigDecimal amount) {
    bill.setAmount(bill.getAmount().add(amount));
  }

  public void minusAmount(Bill bill, BigDecimal amount) {
    if (bill.getAmount().subtract(amount).compareTo(BigDecimal.ZERO) < 0) {
      throw new AmountNotEnoughException();
    }
    bill.setAmount(bill.getAmount().subtract(amount));
  }
}
