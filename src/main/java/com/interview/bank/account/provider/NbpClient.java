package com.interview.bank.account.provider;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.interview.bank.account.dto.provider.out.NbpResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "NBP-Client", url = "${nbp.api.url}")
public interface NbpClient {

  @GetMapping(value = "/exchangerates/rates/a/{currency}", produces = APPLICATION_JSON_VALUE)
  NbpResponse getRates(@PathVariable("currency") String currency);
}
