package com.interview.bank.account.controller;

import com.interview.bank.account.dto.in.AccountCreateRequest;
import com.interview.bank.account.dto.in.ExchangeRequest;
import com.interview.bank.account.dto.out.AccountResponse;
import com.interview.bank.account.service.AccountService;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("account")
public class AccountController {

  private final AccountService accountService;

  public AccountController(AccountService accountService) {
    this.accountService = accountService;
  }

  @PostMapping
  public ResponseEntity<AccountResponse> createAccount(
      @RequestBody @Valid AccountCreateRequest request) {
    return new ResponseEntity<>(accountService.createAccount(request), HttpStatus.CREATED);
  }

  @GetMapping("{pesel}")
  public ResponseEntity<AccountResponse> getAccount(@PathVariable("pesel") String pesel) {
    return new ResponseEntity<>(accountService.getAccount(pesel), HttpStatus.OK);
  }

  @PostMapping("exchange")
  public ResponseEntity<AccountResponse> exchange(@RequestBody @Valid ExchangeRequest request) {
    return new ResponseEntity<>(accountService.exchange(request), HttpStatus.OK);
  }
}
