package com.interview.bank.account.db.enums;

public enum Currency {
  PLN,
  USD
}
