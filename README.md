# INTERVIEW #

INTERVIEW Marek Marszelewski

To build and run the application, run the following commands:

``mvn clean package``

``mvn spring-boot:run``

After starting the application at http://localhost:8888/swagger-ui.html, there is swagger documentation.

The application provides 3 services, to create an account (/account), to exchange currencies (/account/exchange) and to get account information(/account/{PESEL}).
